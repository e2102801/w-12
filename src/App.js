import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="">
      
    <div className="person">
      <div>
      <img src="https://d1csarkz8obe9u.cloudfront.net/posterpreviews/modern-technology-logo-icon-design-template-50fbe83a970337f0b69280bc528f7aa0_screen.jpg?ts=1606039712"/>
      <h2 class="bc">Esther Guilland</h2>
      <p class="bc">TK</p>
      <p class="bc">Erit. kiv. (Ja & Mot. Opskl.)</p>
      <p></p>
      <span><b>Newbie koodari</b></span><br/>
      <span>Tietotekniikka</span><br/>
      <span><b>Opiskelija</b></span><br/>
      <span>H2C</span>
      <p></p>
      <br/><span class="bc">Tekniikan yksikkö</span>
      <br/><span class="bc">School of technology</span>
      <p/>
      <br/><span>esther.guilland@gmail.com</span>
      <br/><span>+358 40 12 51 050</span>
      <p></p>
      <p>Napinvalajantie 6, FI-00620 HELSINKI, Finland</p>
      </div>
    </div>
    </div>
  );
}

export default App;
